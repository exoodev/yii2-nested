<?php

namespace exoo\nested;

/**
 * Asset bundle for widget [[NestedView]].
 */
class NestedViewAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/nested/assets';
    /**
     * @inheritdoc
     */
    public $js = [
        'js/nestable.js',
    ];
    /**
     * @inheritdoc
     */
    public $css = [
        'css/nestable.css',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    	'exoo\nestable\NestableAsset',
    ];
}
