<?php

namespace exoo\nested;

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use exoo\nestable\NestableAsset;

/**
 * Examples:
 *
 * ```
 * <?= NestedView::widget([
 *     'handle' => false,
 *     'modelClass' => $modelClass,
 * ]); ?>
 * ```
 *
 * ```
 * <?= NestedView::widget([
 *     'modelClass' => $modelClass,
 *     'itemContent' => function($id, $item) {
 *         return [
 *             'label' => $item->name,
 *             'url' => ['update', 'id' => $id],
 *             'actions' => [
 *                 [
 *                     'options' => [
 *                         'uk-tooltip' => 'Add subcategory',
 *                         'class' => 'fas fa-indent uk-margin-small-right',
 *                     ],
 *                     'url' => ['create', 'parent' => $id],
 *                 ],
 *                 [
 *                     'url' => ['delete', 'id' => $id],
 *                     'options' => [
 *                         'data' => [
 *                             'method' => 'post',
 *                             'confirm' => 'Are you sure you want to delete?',
 *                         ],
 *                         'class' => 'far fa-trash-alt',
 *                         'uk-tooltip' => 'Delete',
 *                     ],
 *                 ],
 *             ],
 *         ];
 *     }
 * ]); ?>
 * ```
 * @see https://github.com/RamonSmit/Nestable2
 */
class NestedView extends Widget
{
    /**
     * @var array the HTML attributes for the widget container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = [];
    /**
     * @var array the options for the underlying JS plugin.
     */
    public $clientOptions = [];
    /**
    * @var array the HTML attributes
    */
    public $itemsOptions = [];
    /**
    * @var array the HTML attributes
    */
    public $itemOptions = [];
    /**
    * @var array the HTML attributes
    */
    public $handleOptions = [];
    /**
    * @var array the HTML attributes
    */
    public $contentOptions = [];
    /**
    * @var array the HTML attributes
    */
    public $actionsOptions = [];
    /**
     * @var array the HTML attributes
     */
    public $linkOptions = [];
    /**
     * @var boolean whether the label for nestable should be HTML-encoded.
     */
    public $encodeLabels = true;
    /**
     * @var string class name of the model which will be handled by this action.
     * The model class must implement [[ActiveRecordInterface]].
     * This property must be set.
     */
    public $modelClass;
    /**
     * @var array list of items. Each item should be an array of the following structure:
     *
     * - label: string, optional, specifies the item label. When [[encodeLabels]] is true, the label
     *   will be HTML-encoded. If the label is not specified, an empty string will be used.
     * - encode: boolean, optional, whether this item`s label should be HTML-encoded. This param will override
     *   global [[encodeLabels]] param.
     * - items: array, optional, specifies the sub-items. Its format is the same as the parent items.
     * - options: array, optional, the HTML attributes for the item container tag.
     */
    public $items = [];
    /**
     * @var string the move item url
     */
    public $moveUrl = 'move';
    /**
     * @var callable a PHP callable.
     * The signature of the callable should be:
     *
     * ```php
     * function ($id, $item) {
     *     ...
     * }
     * ```
     */
    public $itemContent;
    /**
     * @var boolean the handle
     */
    public $handle = true;
    /**
     * @var boolean
     */
    public $hoverActions = true;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if ($this->modelClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }

        if (empty($this->items)) {
            $this->getItems();
        }

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        $id = $this->options['id'];
        $view = $this->getView();

        NestedViewAsset::register($view);
        NestableAsset::register($view);

        $this->options['data-move-url'] = Url::to([$this->moveUrl]);
        $clientOptions = $this->clientOptions ? Json::encode($this->clientOptions) : '';
        $view->registerJs("Nested.init();");
        $view->registerJs("jQuery('#$id').nestable($clientOptions);");
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        if ($this->hoverActions) {
            Html::addCssClass($this->contentOptions, 'uk-visible-toggle');
            Html::addCssClass($this->actionsOptions, 'uk-hidden-hover');
        }
        Html::addCssClass($this->options, 'dd');
        Html::addCssClass($this->itemsOptions, 'dd-list');
        $items = Html::tag('ol', $this->renderItems($this->items), $this->itemsOptions);
        return Html::tag('div', $items, $this->options);
    }

    /**
     * Recursively renders the nested items.
     * @return string
     */
    public function renderItems($items)
    {
        $result = [];

        foreach ($items as $i => $item) {
            if (!isset($item['label']) || !isset($item['id'])) {
                throw new InvalidConfigException("The 'label' and 'id' options are required.");
            }

            $itemOptions = ArrayHelper::getValue($item, 'itemOptions', $this->itemOptions);
            $itemOptions['data-id'] = $item['id'];
            $node = $this->renderItem($item);

            if (!empty($item['items'])) {
                $node .= Html::tag('ol', $this->renderItems($item['items']), [
                    'class' => 'dd-list'
                ]);
            }
            Html::addCssClass($itemOptions, 'dd-item');

            if ($this->handle) {
                Html::addCssClass($itemOptions, 'dd3-item');
            }

            $result[] = Html::tag('li', $node, $itemOptions);
        }
        return implode("\n", $result);
    }

    /**
     * Renders the content and actions of a nested item.
     * @param array $item
     * @return string
     */
    public function renderItem($item)
    {
        Html::addCssClass($this->handleOptions, 'dd-handle');
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $content = $encodeLabel ? Html::encode($item['label']) : $item['label'];

        if (!empty($item['url'])) {
            $linkOptions = ArrayHelper::getValue($item, 'linkOptions', $this->linkOptions);
            Html::addCssClass($linkOptions, 'dd-nodrag');
            $content = Html::a($content, $item['url'], $linkOptions);
        }

        if (isset($item['actions'])) {
            $result = [
                '<div class="uk-flex uk-flex-between uk-flex-middle uk-grid-collapse" uk-grid>',
                    '<div>' . $content . '</div>',
                    '<div>' . $this->renderActions($item['actions'], $item) . '</div>',
                '</div>',
            ];
            $content = implode("\n", $result);
        }

        $contentOptions = ArrayHelper::getValue($item, 'contentOptions', $this->contentOptions);
        $content = Html::tag('div', $content, $contentOptions);

        if ($this->handle) {
            Html::addCssClass($this->handleOptions, 'dd3-handle');
            $container = "\n" . Html::tag('div', $content, ['class' => 'dd3-content']);
            $content = null;
        } else {
            $container = '';
        }

        return Html::tag('div', $content, $this->handleOptions) . $container;
    }

    /**
     * Render actions
     * @param array $item
     * @return string the result
     */
    public function renderActions($actions, $item)
    {
        $actionsOptions = ArrayHelper::getValue($item, 'actionsOptions', $this->actionsOptions);
        Html::addCssClass($actionsOptions, 'uk-width-auto');
        $list = [];

        foreach ($actions as $action) {
            $icon = isset($action['icon']) ? $action['icon'] : '';
            $label = isset($action['label']) ? $action['label'] : '';
            $options = ArrayHelper::getValue($action, 'options', []);
            Html::addCssClass($options, 'dd-nodrag uk-icon-link');

            if (isset($action['url'])) {
                $node = Html::a($icon . $label, $action['url'], $options);
            } else {
                $tag = ArrayHelper::remove($action, 'tag', 'span');
                $node = Html::tag($tag, $icon . $label, $options);
            }

            $itemOptions = ArrayHelper::getValue($action, 'itemOptions', []);
            $list[] = Html::tag('span', $node, $itemOptions);
        }

        return Html::tag('div', implode("\n", $list), $actionsOptions);
    }

    protected function getItems()
    {
        $items = [];
        $modelClass = $this->modelClass;
        $root = $modelClass::find()->roots()->one();

        if ($root === null) {
            $root = new $modelClass(['name' => 'root']);
            $root->makeRoot();
        }

        if ($items = $root->children()->all()) {
            $this->items = $this->getTree($items);
        }
    }

    protected function getTree($items = [], $left = 0, $right = null, $depth = 1)
    {
        $result = [];
        foreach ($items as $i => $item) {
            if ($item->lft >= $left + 1 && ($item->rgt <= $right || $right === null) && $item->depth == $depth) {
                $id = $item->id;
                $node = [
                    'id' => $id,
                    'label' => $item->name,
                    'items' => $this->getTree($items, $item->lft, $item->rgt, $item->depth + 1),
                ];
                if ($this->itemContent) {
                    $node = array_merge([
                        'id' => $id,
                        'label' => $item->name,
                        'items' => $this->getTree($items, $item->lft, $item->rgt, $item->depth + 1),
                    ], call_user_func($this->itemContent, $id, $item));
                }
                $result[$i] = $node;
            }
        }
        return $result;
    }
}
