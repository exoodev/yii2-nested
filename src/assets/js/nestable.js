var Nested = {
    init: function() {
        $('.dd').nestable({
            callback: function(container, item) {
                var item = $(item);
                var data = {
                    id: item.data('id'),
                    parent: item.parents('.dd-item').data('id'),
                    prev: item.prev().data('id')
                };
                $.post(container.data('move-url'), data).done(function(res) {
                    if (res.message) {
                        UIkit.notification(res.message, res.status);
                    }
                });
            }
        });
    }
}
