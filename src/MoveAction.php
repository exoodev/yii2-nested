<?php

namespace exoo\nested;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;

/**
 * Undocumented class.
 */
class MoveAction extends Action
{
    /**
     * @var string class name of the model which will be handled by this action.
     * The model class must implement [[ActiveRecordInterface]].
     * This property must be set.
     */
    public $modelClass;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if ($this->modelClass === null) {
            throw new InvalidConfigException(get_class($this) . '::$modelClass must be set.');
        }
    }

    /**
     * Undocumented method
     * @param mixed $value
     * @return string the result
     */
    public function run()
    {
        if (!Yii::$app->request->isAjax) return;

        if ($post = Yii::$app->request->post()) {
            $modelClass = $this->modelClass;
            $moveItem = $modelClass::findOne($post['id']);

            if (isset($post['prev'])) {
                $prevItem = $modelClass::findOne($post['prev']);
                return $moveItem->insertAfter($prevItem);
            } elseif (isset($post['parent'])) {
                $parentItem = $modelClass::findOne($post['parent']);
                return $moveItem->prependTo($parentItem);
            } else {
                $root = $modelClass::findOne(['name' => 'root']);
                return $moveItem->prependTo($root);
            }

            return $this->controller->asJson([
                'message' => 'Move error',
                'status' => 'danger'
            ]);
        }
    }
}
